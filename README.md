## VCS Issue Finder

Version Control System issue finder for developers.

## Description
VCS stands for Version Control System and VCS issue finder is a tool that will help
you find projects faster. You might find this particularly useful around October 😉

Furthermore, This project is for developers that are looking to contribute effectively
to one or more code-bases without being restricted to a specific version control system.
This project was created because of the many 'issue finders' I see, none of them have
multi-version-control compatibility, in that they do not work with GitHub, BitBucket, GitLab,
Jira and so on, but rather are incompatible with one or more of these platforms, making it difficult
for users who work with multiple version control systems to use them effectively.

This project is for developers that are looking to contribute effectively to one or more code-bases without being
restricted to a specific version control system. For more information about this project, I strongly suggest you have a look at the
<a src="https://gitlab.com/JoshuaDRose/my-project/-/wikis/home">project wiki</a>; I go into much more detail about what the
project is and all its pertaining information there.

Finally, if you're looking for a usage section, you won't find it in this README document. For information and help on how to use
this tool, I highly suggest having a look at [the wiki](https://gitlab.com/JoshuaDRose/my-project/-/wikis/Usage) which goes into everything there is to know about this tool and how to use it effectively.

## Installation

The installation method is the same on all operating systems, however the method in which you plan to install the
repository is up to you. On this page, I list the most common method; I personally am in favor of using this method.

1. Clone the repository using git
```bash
git clone https://gitlab.com/JoshuaDRose/my-project.git vcs
```
> You can replace 'vcs' with any unoccupied folder name

2. Change directory into the newly cloned project
```bash
cd vcs
```

3. Run any command from the current location
```bash
vcs --help
```

**Post Installation**
 - If you see a help menu, congratulations! This means that your installation was successful.
 - If you don't see a help menu, double check that you're in the correct directory
 and that you have downloaded the files correctly.

## Compatibility
This project plans to be multi-platform compatible with macOS Nix, and Windows.

## Roadmap
Please see the project Milestones for the project roadmap. Milestones are listed with start and end dates, and have 
a list of tasks associated with each one. You can expect these tasks to be completed in the given timeframe.

## Contributing
To contribute to this project please consider the following:
 - Please write tests that cover the code you intend to contribute
 - Please ensure that your code is linted via Black
 - Please make sure that your code is supported with requests-style in-code documentation.
 - Please follow standard commit messages. If unsure, please use previous commit messages as a quick guide.

## License
This open source project is licensed using the MIT software license.
