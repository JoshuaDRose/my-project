"""
VCS Issue Finder - Find issues quickly
Copyright © 2023 Cyré
Licensed under the MIT open source software license.

Author: Cyré
File: docs/source/__init__.py
"""

from source import conf  # pyright: ignore
