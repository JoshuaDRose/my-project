"""
VCS Issue Finder - Find issues quickly
Copyright © 2023 Cyré
Licensed under the MIT open source software license.

Author: Cyré
File: vcs/__main__.py

This file handles the processing of arguments
by calling parse_args() and performing additional
safety checks before showing output.
"""

import logging

import api
import utils

parser = utils.Parser()

utils.declare_basic_config()  # pyright: ignore

logger = logging.getLogger(__name__)


def main():
    args = parser.parse_args()
    repos = []

    console = utils.Console()

    try:
        if args.sort[0] not in [
            "best_match",
            "stars",
            "forks",
            "help-wanted-issues",
            "good-first-issues",
        ]:
            parser.error(f"Invalid sorting method: {args.sort[0]}")
    except IndexError:
        parser.error("You must specify only one sorting method")

    args.desc = (
        args.desc[0] if isinstance(args.desc, list) and len(args.desc) == 1 else ""
    )

    args.order = (
        "descending"
        if isinstance(args.descending, list) and len(args.descending) == 1
        else (
            "ascending"
            if isinstance(args.descending, list) and len(args.ascending)
            else "descending"
        )
    )

    GitHubAPI = api.GitHub()
    repos = GitHubAPI.find_issues(
        search_query=args.desc, sort=args.sort[0], order=args.order
    )

    logger.debug(f"description: {args.desc}")
    logger.debug(f"sort: {args.sort[0]}")
    logger.debug(f"order (modified): {args.order}")

    console.display_repository_information_as_group(repos)


if __name__ == "__main__":
    main()
