"""
VCS Issue Finder - Find issues quickly
Copyright © 2023 Cyré
Licensed under the MIT open source software license.

Author: Cyré
File: vcs/api.py

This file contains a class that handles APIs for:
    - GitHub
    - GitLab (TBD)

Why this over a library for github/gitlab? Token-less site scraping :)
"""

import logging
import sys

import requests

try:
    import utils

    utils.declare_basic_config()  # pyright: ignore
except ImportError:
    from .utils import Repository, declare_basic_config, Owner  # pyright: ignore

    declare_basic_config()

global logger


class GitHub(object):
    """GitHub API to index repositories via their name, issues ..."""

    API_URL_BASE = "https://api.github.com"
    SEARCH_URL = f"{API_URL_BASE}/search/repositories"

    def __init__(self):
        self._api_url = GitHub.API_URL_BASE
        self.console = utils.Repository.console

    def find_issues(
        self,
        search_query: str,
        sort: str = "best_match",
        order: str = "desc",
        test_case: bool = False,
    ):
        """Find GitHub issues based on the given parameters.

        Args:
            search_query (str): The search query to find GitHub issues.
            sort (str, optional): The sort order of the search results. Defaults to "best_match".
            order (str, optional): The order of the search results. Defaults to "desc".

        Returns:
            List[class`Repository`]: A list of dictionaries representing the found GitHub issues.
        """

        with self.console.status("Fetching from " + self._api_url):
            response = requests.get(
                GitHub.SEARCH_URL, params={"q": search_query, "sort": sort, "order": order}
            )

        data = response.json()

        if "message" in data.keys() and data["message"] == "Validation Failed":
            logger.critical("Something is fucked")
            sys.exit(100)

        if "items" in data.keys():
            data_repositories = data["items"]
        else:
            data_repositories = data

        _repositories = []

        if not test_case:
            for value in data_repositories:
                owner = utils.Owner(
                    id=value["owner"]["id"],
                    type=value["owner"]["type"],
                    login=value["owner"]["login"],
                    node_id=value["owner"]["node_id"],
                    avatar_url=value["owner"]["avatar_url"],
                    gravatar_id=value["owner"]["gravatar_id"],
                    url=value["owner"]["url"],
                    html_url=value["owner"]["html_url"],
                    followers_url=value["owner"]["followers_url"],
                    following_url=value["owner"]["following_url"],
                    gists_url=value["owner"]["gists_url"],
                    repos_url=value["owner"]["repos_url"],
                    starred_url=value["owner"]["starred_url"],
                    site_admin=value["owner"]["site_admin"],
                )

                repo = utils.Repository(
                    id=value["id"],
                    node_id=value["node_id"],
                    license=value["license"],
                    owner=owner,
                    name=value["name"],
                    full_name=value["full_name"],
                    default_branch=value["default_branch"],
                    open_issues_count=value["open_issues_count"],
                    html_url=value["html_url"],
                    description=value["description"],
                    fork=value["fork"],
                    topics=value["topics"],
                    url=value["url"],
                    forks_url=value["forks_url"],
                    keys_url=value["keys_url"],
                    collaborators_url=value["collaborators_url"],
                    teams_url=value["teams_url"],
                    hooks_url=value["hooks_url"],
                    issue_events_url=value["issue_events_url"],
                    events_url=value["events_url"],
                    assignees_url=value["assignees_url"],
                    branches_url=value["branches_url"],
                    tags_url=value["tags_url"],
                    blobs_url=value["blobs_url"],
                    git_tags_url=value["git_tags_url"],
                    git_refs_url=value["git_refs_url"],
                    trees_url=value["trees_url"],
                    statuses_url=value["statuses_url"],
                    languages_url=value["languages_url"],
                    stargazers_url=value["stargazers_url"],
                    contributors_url=value["contributors_url"],
                    subscribers_url=value["subscribers_url"],
                    subscription_url=value["subscription_url"],
                    commits_url=value["commits_url"],
                    issue_comment_url=value["issue_comment_url"],
                    contents_url=value["contents_url"],
                    compare_url=value["compare_url"],
                    merges_url=value["merges_url"],
                    archive_url=value["archive_url"],
                    downloads_url=value["downloads_url"],
                    issues_url=value["issues_url"],
                    pulls_url=value["pulls_url"],
                    milestones_url=value["milestones_url"],
                    notifications_url=value["notifications_url"],
                    labels_url=value["labels_url"],
                    releases_url=value["releases_url"],
                    deployments_url=value["deployments_url"],
                    mirror_url=value["mirror_url"],
                    updated_at=value["updated_at"],
                    created_at=value["created_at"],
                    pushed_at=value["pushed_at"],
                    git_url=value["git_url"],
                    ssh_url=value["ssh_url"],
                    clone_url=value["clone_url"],
                    homepage=value["homepage"],
                    size=value["size"],
                    archived=value["archived"],
                    disabled=value["disabled"],
                    allow_forking=value["allow_forking"],
                    web_commit_signoff_required=value["web_commit_signoff_required"],
                    forks=value["forks"],
                    stargazers_count=value["stargazers_count"],
                    watchers_count=value["watchers_count"],
                    language=value["language"],
                    forks_count=value["forks_count"],
                    has_projects=value["has_projects"],
                    has_wiki=value["has_wiki"],
                    has_downloads=value["has_downloads"],
                    has_pages=value["has_pages"],
                    has_discussions=value["has_discussions"],
                    private=value["private"],
                )
                _repositories.append(repo)

        else:
            for value in data_repositories:
                owner = Owner(
                    id=value["owner"]["id"],
                    type=value["owner"]["type"],
                    login=value["owner"]["login"],
                    node_id=value["owner"]["node_id"],
                    avatar_url=value["owner"]["avatar_url"],
                    gravatar_id=value["owner"]["gravatar_id"],
                    url=value["owner"]["url"],
                    html_url=value["owner"]["html_url"],
                    followers_url=value["owner"]["followers_url"],
                    following_url=value["owner"]["following_url"],
                    gists_url=value["owner"]["gists_url"],
                    repos_url=value["owner"]["repos_url"],
                    starred_url=value["owner"]["starred_url"],
                    site_admin=value["owner"]["site_admin"],
                )

                repo = Repository(
                    id=value["id"],
                    node_id=value["node_id"],
                    license=value["license"],
                    owner=owner,
                    name=value["name"],
                    full_name=value["full_name"],
                    default_branch=value["default_branch"],
                    open_issues_count=value["open_issues_count"],
                    html_url=value["html_url"],
                    description=value["description"],
                    fork=value["fork"],
                    topics=value["topics"],
                    url=value["url"],
                    forks_url=value["forks_url"],
                    keys_url=value["keys_url"],
                    collaborators_url=value["collaborators_url"],
                    teams_url=value["teams_url"],
                    hooks_url=value["hooks_url"],
                    issue_events_url=value["issue_events_url"],
                    events_url=value["events_url"],
                    assignees_url=value["assignees_url"],
                    branches_url=value["branches_url"],
                    tags_url=value["tags_url"],
                    blobs_url=value["blobs_url"],
                    git_tags_url=value["git_tags_url"],
                    git_refs_url=value["git_refs_url"],
                    trees_url=value["trees_url"],
                    statuses_url=value["statuses_url"],
                    languages_url=value["languages_url"],
                    stargazers_url=value["stargazers_url"],
                    contributors_url=value["contributors_url"],
                    subscribers_url=value["subscribers_url"],
                    subscription_url=value["subscription_url"],
                    commits_url=value["commits_url"],
                    issue_comment_url=value["issue_comment_url"],
                    contents_url=value["contents_url"],
                    compare_url=value["compare_url"],
                    merges_url=value["merges_url"],
                    archive_url=value["archive_url"],
                    downloads_url=value["downloads_url"],
                    issues_url=value["issues_url"],
                    pulls_url=value["pulls_url"],
                    milestones_url=value["milestones_url"],
                    notifications_url=value["notifications_url"],
                    labels_url=value["labels_url"],
                    releases_url=value["releases_url"],
                    deployments_url=value["deployments_url"],
                    mirror_url=value["mirror_url"],
                    updated_at=value["updated_at"],
                    created_at=value["created_at"],
                    pushed_at=value["pushed_at"],
                    git_url=value["git_url"],
                    ssh_url=value["ssh_url"],
                    clone_url=value["clone_url"],
                    homepage=value["homepage"],
                    size=value["size"],
                    archived=value["archived"],
                    disabled=value["disabled"],
                    allow_forking=value["allow_forking"],
                    web_commit_signoff_required=value["web_commit_signoff_required"],
                    forks=value["forks"],
                    stargazers_count=value["stargazers_count"],
                    watchers_count=value["watchers_count"],
                    language=value["language"],
                    forks_count=value["forks_count"],
                    has_projects=value["has_projects"],
                    has_wiki=value["has_wiki"],
                    has_downloads=value["has_downloads"],
                    has_pages=value["has_pages"],
                    has_discussions=value["has_discussions"],
                    private=value["private"],
                )
                _repositories.append(repo)

        return _repositories


logger = logging.getLogger(__name__)
