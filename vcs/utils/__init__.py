"""
VCS Issue Finder - Find issues quickly
Copyright © 2023 Cyré
Licensed under the MIT open source software license.

Author: Cyré
File: vcs/utils/__init__.py

This file adds various paths to the system path
so that the tests folder can access it.

Additionally, this folder reduces namespace for
import in other files
"""

import os
import sys

sys.path.append(os.path.join(os.getcwd()))
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), "../..", "docs", "source"))
)

from vcs.utils.parser import ParserVCS as Parser  # pyright: ignore
from vcs.utils.repository import Repository, Owner  # pyright: ignore
from vcs.utils.logger import declare_basic_config  # pyright: ignore
from vcs.utils.display import Console  # pyright: ignore
