"""
VCS Issue Finder - Find issues quickly
Copyright © 2023 Cyré
Licensed under the MIT open source software license.

Author: Cyré
File: vcs/utils/display.py

Houses the 'Console' for rich.
"""

from datetime import datetime
from typing import Callable, Literal, Mapping, Optional

from rich.columns import Columns
from rich.console import Console as rich_console, HighlighterType
from rich.emoji import EmojiVariant
from rich.panel import Panel
from rich.style import StyleType
from rich.theme import Theme


class Console(rich_console):
    def __init__(
        self,
        *,
        color_system: Optional[
            Literal["auto", "standard", "256", "truecolor", "windows"]
        ] = "auto",
        force_terminal: Optional[bool] = None,
        force_jupyter: Optional[bool] = None,
        force_interactive: Optional[bool] = None,
        soft_wrap: bool = False,
        theme: Optional[Theme] = None,
        stderr: bool = False,
        quiet: bool = False,
        width: Optional[int] = None,
        height: Optional[int] = None,
        style: Optional[StyleType] = None,
        no_color: Optional[bool] = None,
        tab_size: int = 8,
        record: bool = False,
        markup: bool = True,
        emoji: bool = True,
        emoji_variant: Optional[EmojiVariant] = None,
        highlight: bool = True,
        log_time: bool = True,
        log_path: bool = True,
        log_time_format: str = "[%X]",
        highlighter: Optional["HighlighterType"] = ...,
        legacy_windows: Optional[bool] = None,
        safe_box: bool = True,
        get_datetime: Optional[Callable[[], datetime]] = None,
        get_time: Optional[Callable[[], float]] = None,
        _environ: Optional[Mapping[str, str]] = None,
    ):
        super().__init__(
            color_system=color_system,
            force_terminal=force_terminal,
            force_jupyter=force_jupyter,
            force_interactive=force_interactive,
            soft_wrap=soft_wrap,
            theme=theme,
            stderr=stderr,
            quiet=quiet,
            width=width,
            height=height,
            style=style,
            no_color=no_color,
            tab_size=tab_size,
            record=record,
            markup=markup,
            emoji=emoji,
            emoji_variant=emoji_variant,
            highlight=highlight,
            log_time=log_time,
            log_path=log_path,
            log_time_format=log_time_format,
            highlighter=highlighter,
            legacy_windows=legacy_windows,
            safe_box=safe_box,
            get_datetime=get_datetime,
            get_time=get_time,
            _environ=_environ,
        )

        self.width = 40
        self.height = 10

    def display_repository_information(self, _repository):
        """Print repository information

        :param _repository: Repository class as an instance
        :type _repository: `Repository`
        """

        renderables = [
            Panel(
                f"""[b][cyan]{_repository.owner.login}[/cyan][/b]/[blue]{_repository.name}[blue]

 ⭐ [b][yellow]{_repository.stargazers_count}[/yellow][/b]

 👀 [b][white]{_repository.watchers_count}[/white][/b]

 🚩 [b][red]{_repository.open_issues_count}[/red][/b]
""",
                expand=True,
                width=self.width,
                height=self.height,
                subtitle_align="center",
                # title=f"[b]{_repository.name}[/b],
            )
        ]

        self.print(
            Columns(
                renderables,
                equal=True,
                column_first=True,
            )
        )

    def display_repository_information_as_group(self, _repositories):
        """Print repository information as a group of cells

        :param _repository: Repository class as an instance
        :type _repository: `Repository`
        """

        renderables = [
            Panel(
                f"""[b][cyan][link={repo.owner.html_url}]{repo.owner.login}[/link][/cyan][/b]/[blue]{repo.name}[blue]

 ⭐ [b][yellow]{repo.stargazers_count}[/yellow][/b]

 👀 [b][white]{repo.watchers_count}[/white][/b]

 🚩 [b][red]{repo.open_issues_count}[/red][/b]
""",
                expand=True,
                width=self.width,
                height=self.height,
            )
            for repo in _repositories
        ]

        self.print(
            Columns(
                renderables,
                equal=True,
            ),
            crop=False,
        )
