"""
VCS Issue Finder - Find issues quickly
Copyright © 2023 Cyré
Licensed under the MIT open source software license.

Author: Cyré
File: vcs/utils/owner.py

This file contains the Owner class which is used
to represent the GitHub owner of a repo.
"""

from dataclasses import dataclass


@dataclass
class Owner:
    """Data structure to represent the owner of an indexed repository"""

    def __init__(
        self,
        id,
        type,
        login,
        node_id,
        avatar_url,
        gravatar_id,
        url,
        html_url,
        followers_url,
        following_url,
        gists_url,
        site_admin,
        repos_url,
        starred_url,
        ) -> None:

        self.id: int = id
        self.type: str = type

        self.login: str = login
        self._node_id: str = node_id
        self.avatar_url: str = avatar_url
        self.gravatar_id: str = gravatar_id

        self.url: str = url
        self.html_url: str = html_url

        self.followers_url: str = followers_url
        self.following_url: str = following_url

        self.gists_url: str = gists_url
        self.site_admin: bool = site_admin

        self.repos_url: str = repos_url
        self.starred_url: str = starred_url

    @property
    def node_id(self):
        """
        Get the unique identifier of the repository in the GitHub API.

        Returns:
            str: The unique node ID of the repository.
        """
        return self._node_id

    @node_id.setter
    def node_id(self, value):
        """
        Set the unique identifier of the repository in the GitHub API.

        Args:
            value (str): The unique node ID of the repository.
        """
        self._node_id = value
