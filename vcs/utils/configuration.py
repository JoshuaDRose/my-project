"""
VCS Issue Finder - Find issues quickly
Copyright © 2023 Cyré
Licensed under the MIT open source software license.

Author: Cyré
File: vcs/utils/configuration.py

This file contains a configuration class which represents
the conf.py for documentation.
"""

import conf  # pyright: ignore
from dataclasses import dataclass
from .logger import declare_basic_config


@dataclass
class _Version:
    """
    Private class to allow a cleaner __repr__ method.
    (used in command line when version number is called)

    :param version: Version as defined in any instance of `Configuration._version`
    """

    version: str

    def __repr__(self) -> str:
        return f"VCS version {self.version}"


class Configuration:
    """
    Configuration class to represent docs/source/conf.py
    """

    def __init__(self):
        self._version = conf.release

    @property
    def version(self) -> str:
        """Return the current version.

        This static method reads the documentation configuration
        to identify the current version of this project.
        """
        return self._version

    @version.setter
    def version(self, value):
        self._version = value

    @version.getter
    def version(self):
        return conf.release

    @property
    def version_info(self):
        return _Version(self.version)


if __name__ == "__main__":
    declare_basic_config()
