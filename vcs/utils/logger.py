"""
VCS Issue Finder - Find issues quickly
Copyright © 2023 Cyré
Licensed under the MIT open source software license.

Author: Cyré
File: vcs/utils/logger.py

This file contains functions that pertain to logging.
"""

import sys
import logging
from rich.logging import RichHandler


def declare_basic_config():
    """Declares a basic configuration for logger.

    This prevents having to declare a lengthy config in each file, and the following can just be called instead:
    >>> if __name__ == '__main__':
    >>>     declare_basic_config()
    """

    log_level = logging.DEBUG if "-v" or "--verbose" in sys.argv else logging.INFO

    logging.basicConfig(
        level=log_level,
        format="%(asctime)s %(name)-12s %(levelname)-8s %(message)s",
        datefmt="%H:%M:%S",
        handlers=[RichHandler()],
    )
