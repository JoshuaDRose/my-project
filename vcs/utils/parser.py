"""
VCS Issue Finder - Find issues quickly
Copyright © 2023 Cyré
Licensed under the MIT open source software license.

File: vcs/utils/parser.py
Author: Cyré

This file uses the argparse library to
parse various parameters that can be specified
into the command line, some optional, some mandatory.
"""

import argparse
from .logger import declare_basic_config

from .configuration import Configuration


class ParserVCS(argparse.ArgumentParser):
    """Contains methods that process command line arguments"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.configuration = Configuration()

        self.search_filters = self.add_mutually_exclusive_group(required=True)
        self.order = self.add_mutually_exclusive_group(required=False)

        # Version
        self.add_argument(
            "--version",
            action="version",
            version=self.configuration.version_info.__repr__(),
            default=argparse.SUPPRESS,
        )

        # output style
        self.add_argument(
            "--sort",
            nargs=1,
            required=True,
            help="Specify sorting method: best_match, stars, forks, help-wanted-issues, good-first-issues",
        )

        # Repo-indexing
        self.search_filters.add_argument(
            "--desc",
            nargs=1,
            help="Search GitHub for a specific repository description",
        )

        self.search_filters.add_argument(
            "--author",
            nargs=1,
            help="Search GitHub for an author",
        )

        self.add_argument(
            "--stars",
            required=False,
            help="Index repositories by star range.\neg: stars 10",
        )

        # Additional Filtering
        self.order.add_argument(
            "--ascending",
            help="Display results in ascending order",
            action="store_true",
        )

        self.order.add_argument(
            "--descending",
            help="Display results in descending order",
            action="store_true",
        )

        self.add_argument(
            "--verbose",
            help="Increase verbosity of output logs",
            action="store_true",
        )


if __name__ == "__main__":
    declare_basic_config()
