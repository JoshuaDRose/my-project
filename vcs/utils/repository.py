"""
VCS Issue Finder - Find issues quickly
Copyright © 2023 Cyré
Licensed under the MIT open source software license.

Author: Cyré
File: vcs/utils/repository.py

This file contains the Repository class which is used
to represent indexed repositories.
"""

from dataclasses import dataclass
import re
import requests

from .logger import declare_basic_config
from .owner import Owner
from .display import Console


@dataclass
class Repository:
    """data structure to represent indexed repositories."""

    console = Console(width=80)

    def __init__(
        self,
        id: int,
        node_id: str,
        license: str,
        owner: Owner,
        name: str,
        full_name: str,
        default_branch: str,
        open_issues_count: int,
        html_url: str,
        description: None | str,
        fork: bool,
        topics: list[str] | list,
        url: str,
        forks_url: str,
        keys_url: str,
        collaborators_url: str,
        teams_url: str,
        hooks_url: str,
        issue_events_url: str,
        events_url: str,
        assignees_url: str,
        branches_url: str,
        tags_url: str,
        blobs_url: str,
        git_tags_url: str,
        git_refs_url: str,
        trees_url: str,
        statuses_url: str,
        languages_url: str,
        stargazers_url: str,
        contributors_url: str,
        subscribers_url: str,
        subscription_url: str,
        commits_url: str,
        issue_comment_url: str,
        contents_url: str,
        compare_url: str,
        merges_url: str,
        archive_url: str,
        downloads_url: str,
        issues_url: str,
        pulls_url: str,
        milestones_url: str,
        notifications_url: str,
        labels_url: str,
        releases_url: str | None,
        deployments_url: None | str,
        mirror_url: None | str,
        updated_at: str,
        created_at: str,
        pushed_at: str,
        git_url: str,
        ssh_url: str,
        clone_url: str,
        homepage: str | None,
        size: int,
        archived: bool,
        disabled: bool,
        allow_forking: bool,
        web_commit_signoff_required: bool,
        forks: int,
        stargazers_count: int,
        watchers_count: int,
        language: str,
        forks_count: int,
        has_projects: bool,
        has_wiki: bool,
        has_downloads: bool,
        has_pages: bool,
        has_discussions: bool,
        private: bool = False,
    ):
        """Initialize a Repository object with the fetched GitHub repository data.

        Args:
            id (int): The unique identifier of the repository.
            node_id (str): The unique node identifier of the repository.
            license (str): The license key of the repository.
            owner (Owner): The owner of the repository.
            name (str): The name of the repository.
            full_name (str): The full name of the repository.
            default_branch (str): The default branch of the repository.
            open_issues_count (int): The count of open issues in the repository.
            html_url (str): The URL of the repository on GitHub.
            description (None | str): The description of the repository, or None if not available.
            fork (bool): True if the repository is a fork, False otherwise.
            topics (list[str] | list): The topics or tags associated with the repository.
            url (str): The URL of the repository.
            forks_url (str): The URL for fetching forks of the repository.
            keys_url (str): The URL for fetching the repository's keys.
            collaborators_url (str): The URL for fetching the repository's collaborators.
            teams_url (str): The URL for fetching the repository's teams.
            hooks_url (str): The URL for fetching the repository's hooks.
            issue_events_url (str): The URL for fetching the repository's issue events.
            events_url (str): The URL for fetching the repository's events.
            assignees_url (str): The URL for fetching the repository's assignees.
            branches_url (str): The URL for fetching the repository's branches.
            tags_url (str): The URL for fetching the repository's tags.
            blobs_url (str): The URL for fetching the repository's blobs.
            git_tags_url (str): The URL for fetching the repository's git tags.
            git_refs_url (str): The URL for fetching the repository's git refs.
            trees_url (str): The URL for fetching the repository's trees.
            statuses_url (str): The URL for fetching the repository's statuses.
            languages_url (str): The URL for fetching the repository's languages.
            stargazers_url (str): The URL for fetching the repository's stargazers.
            contributors_url (str): The URL for fetching the repository's contributors.
            subscribers_url (str): The URL for fetching the repository's subscribers.
            subscription_url (str): The URL for subscribing to the repository.
            commits_url (str): The URL for fetching the repository's commits.
            issue_comment_url (str): The URL for fetching the repository's issue comments.
            contents_url (str): The URL for fetching the repository's contents.
            compare_url (str): The URL for comparing the repository.
            merges_url (str): The URL for fetching the repository's merges.
            archive_url (str): The URL for fetching the repository's archive.
            downloads_url (str): The URL for fetching the repository's downloads.
            issues_url (str): The URL for fetching the repository's issues.
            pulls_url (str): The URL for fetching the repository's pull requests.
            milestones_url (str): The URL for fetching the repository's milestones.
            notifications_url (str): The URL for fetching the repository's notifications.
            labels_url (str): The URL for fetching the repository's labels.
            releases_url (str | None): The URL for fetching the repository's releases, or None if not available.
            deployments_url (None | str): The URL for fetching the repository's deployments, or None if not available.
            mirror_url (None | str): The URL of the repository's mirror, or None if not available.
            updated_at (str): The last updated timestamp of the repository.
            created_at (str): The creation timestamp of the repository.
            pushed_at (str): The timestamp of the last push to the repository.
            git_url (str): The Git URL of the repository.
            ssh_url (str): The SSH URL of the repository.
            clone_url (str): The clone URL of the repository.
            homepage (str | None): The homepage URL of the repository, or None if not available.
            size (int): The size of the repository in kilobytes.
            archived (bool): True if the repository is archived, False otherwise.
            disabled (bool): True if the repository is disabled, False otherwise.
            allow_forking (bool): True if forking is allowed in the repository, False otherwise.
            web_commit_signoff_required (bool): True if web commit sign-off is required in the repository, False otherwise.
            forks (int): The count of forks of the repository.
            stargazers_count (int): The count of stargazers of the repository.
            watchers_count (int): The count of watchers of the repository.
            language (str): The primary programming language used in the repository.
            forks_count (int): The count of forks of the repository.
            has_projects (bool): True if the repository has projects, False otherwise.
            has_wiki (bool): True if the repository has a wiki, False otherwise.
            has_downloads (bool): True if the repository has downloads, False otherwise.
            has_pages (bool): True if the repository has pages, False otherwise.
            has_discussions (bool): True if the repository has discussions, False otherwise.
            private (bool, optional): True if the repository is private, False if it is public. Defaults to False.

        Returns:
            None.

        Raises:
            None.
        """

        self.id = id

        self._node_id = node_id
        self._license: str = license
        self.owner: Owner = owner

        self._name: str = name
        self._full_name: str = full_name
        self._default_branch: str = default_branch

        self._open_issues_count: int = open_issues_count
        self._html_url: str = html_url
        self._description: None | str = description
        self._fork: bool = fork

        self._topics: list[str] | list = topics

        # urls
        self._url: str = url
        self._forks_url: str = forks_url
        self._keys_url: str = keys_url

        self._collaborators_url: str = collaborators_url
        self._teams_url: str = teams_url
        self._hooks_url: str = hooks_url
        self._issue_events_url: str = issue_events_url
        self._events_url: str = events_url
        self._assignees_url: str = assignees_url
        self._branches_url: str = branches_url
        self._tags_url: str = tags_url
        self._blobs_url: str = blobs_url
        self._git_tags_url: str = git_tags_url
        self._git_refs_url: str = git_refs_url
        self._trees_url: str = trees_url
        self._statuses_url: str = statuses_url
        self._languages_url: str = languages_url
        self._stargazers_url: str = stargazers_url
        self._contributors_url: str = contributors_url
        self._subscribers_url: str = subscribers_url
        self._subscription_url: str = subscription_url
        self._commits_url: str = commits_url
        self._issue_comment_url: str = issue_comment_url
        self._contents_url: str = contents_url
        self._compare_url: str = compare_url
        self._merges_url: str = merges_url
        self._archive_url: str = archive_url
        self._downloads_url: str = downloads_url
        self._issues_url: str = issues_url
        self._pulls_url: str = pulls_url
        self._milestones_url: str = milestones_url
        self._notifications_url: str = notifications_url
        self._labels_url: str = labels_url
        self._releases_url: str | None = releases_url
        self._deployments_url: None | str = deployments_url
        self._mirror_url: None | str = mirror_url

        self._updated_at: str = updated_at
        self._created_at: str = created_at
        self._pushed_at: str = pushed_at

        self._git_url: str = git_url
        self._ssh_url: str = ssh_url
        self._clone_url: str = clone_url

        self._homepage: str | None = homepage
        self._size: int = size
        self._archived: bool = archived
        self._disabled: bool = disabled
        self._allow_forking: bool = allow_forking
        self._web_commit_signoff_required: bool = web_commit_signoff_required
        self._forks: int = forks

        # public info
        self._stargazers_count: int = stargazers_count
        self._watchers_count: int = watchers_count
        self._language: str = language
        self._forks_count: int = forks_count

        self._has_projects: bool = has_projects
        self._has_wiki: bool = has_wiki
        self._has_downloads: bool = has_downloads
        self._has_pages: bool = has_pages
        self._has_discussions: bool = has_discussions

        self._private: bool = private

        self.console = Console(width=80)

    @property
    def node_id(self):
        """
        Get the unique identifier of the repository in the GitHub API.

        Returns:
            str: The unique node ID of the repository.
        """
        return self._node_id

    @node_id.setter
    def node_id(self, value):
        """
        Set the unique identifier of the repository in the GitHub API.

        Args:
            value (str): The unique node ID of the repository.
        """
        if not isinstance(value, str):
            raise ValueError
        self._node_id = value

    @property
    def license(self):
        """
        License information for the repository.

        Returns:
            dict: A dictionary containing details about the license, such as the key, name, SPDX ID, URL, and node ID.
        """
        return self._license

    @license.setter
    def license(self, value):
        """
        Setter for the license property.

        Args:
            value (dict): A dictionary containing details about the license, such as the key, name, SPDX ID, URL, and node ID.
        """
        if not isinstance(value, str):
            raise ValueError
        self._license = value

    @property
    def name(self):
        """
        Get the name of the repository.

        Returns:
            str: The name of the repository.
        """
        return self._name

    @name.setter
    def name(self, value):
        """
        Set the name of the repository.

        Args:
            value (str): The new name for the repository.
        """
        if not isinstance(value, str):
            raise ValueError
        self._name = value

    @property
    def full_name(self) -> str:
        """Gets the full name of the repository.

        Returns:
            str: The full name of the repository, including the owner's username or organization
                name and the repository name, in the format 'owner/repo'.
        """
        return self._full_name

    @full_name.setter
    def full_name(self, value: str):
        """Sets the full name of the repository.

        Args:
            value (str): The full name of the repository, including the owner's username or organization
                name and the repository name, in the format 'owner/repo'.
        """
        if not isinstance(value, str):
            raise ValueError
        self._full_name = value

    @property
    def default_branch(self) -> str:
        """Get the default branch of the repository.

        Returns:
            str: The name of the default branch.

        Raises:
            AttributeError: If the default branch is not available.

        Notes:
            The default branch is the primary branch of the repository where most development work occurs.
            It is typically the 'master' branch, but can be customized by repository owners.

        Example:
            >>> repo = GitHubRepository()
            >>> repo.default_branch
            'master'
        """
        return self._default_branch

    @default_branch.setter
    def default_branch(self, branch: str):
        """Set the default branch of the repository.

        Args:
            branch (str): The name of the default branch.

        Raises:
            ValueError: If the provided branch name is empty or invalid.

        Example:
            >>> repo = GitHubRepository()
            >>> repo.default_branch = 'main'
        """
        if not isinstance(branch, str):
            raise ValueError
        if not branch:
            raise ValueError("Branch name cannot be empty.")
        self._default_branch = branch

    @property
    def open_issues_count(self):
        """Get the count of open issues.

        Returns:
            int: The number of open issues for the repository.

        Raises:
            None.

        Examples:
            Usage example::

                repository = GitHubRepository()
                print(repository.open_issues_count)  # Output: 10

        """
        return self._open_issues_count

    @open_issues_count.setter
    def open_issues_count(self, value):
        """Set the count of open issues.

        Args:
            value (int): The new count of open issues to be set.

        Returns:
            None.

        Raises:
            ValueError: If the provided value is not an integer or is negative.

        Examples:
            Usage example::

                repository = GitHubRepository()
                repository.open_issues_count = 15

        """
        if not isinstance(value, int) or value < 0:
            raise ValueError("Open issues count must be a non-negative integer.")
        self._open_issues_count = value

    @property
    def html_url(self):
        """The HTML URL of the GitHub repository.

        Returns:
            str: The URL as a string pointing to the web page of the GitHub repository.
        """
        return self._html_url

    @html_url.setter
    def html_url(self, value):
        """Set the HTML URL of the GitHub repository.

        Args:
            value (str): The URL as a string pointing to the web page of the GitHub repository.
        """

        if not isinstance(value, str):
            raise ValueError
        self._html_url = value

    @property
    def description(self):
        """Gets the description of the repository.

        Returns:
            str: The description of the repository, or None if not available.

        Raises:
            None

        Examples:
            Usage of the `description` property:
                >>> repo = Repository()
            >>> print(repo.description)
            "This is a sample repository."

        """
        return self._description

    @description.setter
    def description(self, value):
        """Sets the description of the repository.

        Args:
            value (str): The new description for the repository.

        Returns:
            None

        Raises:
            None

        Examples:
            Usage of the `description` property setter:
                >>> repo = Repository()
            >>> repo.description = "This is an updated description."

        """
        if not isinstance(value, str | None):
            raise ValueError("Repo description must be a string or None")
        self._description = value

    @property
    def fork(self):
        """Get whether the repository is a fork.

        Returns:
            bool: True if the repository is a fork, False otherwise.
        """
        return self._fork

    @fork.setter
    def fork(self, value):
        """Set whether the repository is a fork.

        Args:
            value (bool): The value indicating whether the repository is a fork.
        """
        if not isinstance(value, bool):
            raise ValueError("Fork must be boolean.")
        self._fork = value

    @property
    def topics(self):
        """List[str]: Get the topics associated with the repository.

        This property retrieves the topics (keywords or tags) associated with the repository
        as fetched from the GitHub API. The topics provide additional metadata about the
        repository's content and can help in categorization or searchability.

        Returns:
            List[str]: A list of topics associated with the repository.

        Examples:
            >>> my_repo = MyClass()
            >>> my_repo.topics
            ['python', 'open-source', 'github']

        """
        return self._topics

    @topics.setter
    def topics(self, value):
        """Set the topics associated with the repository.

        This setter allows you to update the topics associated with the repository.
        The `value` parameter should be a list of strings representing the desired topics.

        Args:
            value (List[str]): A list of topics to associate with the repository.

        Examples:
            >>> my_repo = MyClass()
            >>> my_repo.topics = ['python', 'open-source', 'github']
            >>> my_repo.topics
            ['python', 'open-source', 'github']

        """
        if not isinstance(value, list):
            raise ValueError(
                f"Invalid topic format provided: must be list, found {type(value)}"
            )
        self._topics = value

    @property
    def url(self):
        """The URL of the GitHub repository.

        Returns:
            str: The URL of the repository.

        Raises:
            ValueError: If the URL is not available or invalid.
        """
        return self._url

    @url.setter
    def url(self, value):
        """Sets the URL of the GitHub repository.

        Args:
            value (str): The URL to set.

        Raises:
            ValueError: If the URL is invalid or does not match the expected format.
        """
        if not value:
            raise ValueError("URL must be provided.")

        # Check if the URL matches expected format
        if not self.validate_url(value):
            raise ValueError("Invalid URL format for url.")

        self._url = value

    @property
    def forks_url(self):
        """The URL for fetching the forks of the repository.

        Returns:
            str: The URL for fetching the forks of the repository.

        Raises:
            None.
        """
        return self._forks_url

    @forks_url.setter
    def forks_url(self, value):
        """Sets the URL for fetching the forks of the repository.

        Args:
            value (str): The URL for fetching the forks of the repository.

        Returns:
            None.

        Raises:
            None.
        """
        if not isinstance(value, str):
            raise ValueError("URL must be a string.")
        self._forks_url = value

    @property
    def keys_url(self):
        """The URL for fetching the keys associated with the user.

        Returns:
            str: The keys URL.

        Raises:
            None.
        """
        return self._keys_url

    @keys_url.setter
    def keys_url(self, value):
        """Sets the URL for fetching the keys associated with the user.

        Args:
            value (str): The keys URL to set.

        Returns:
            None.

        Raises:
            None.
        """
        if not isinstance(value, str):
            raise ValueError("URL must be a string.")
        self._keys_url = value

    @property
    def collaborators_url(self):
        """The URL for retrieving collaborators of the GitHub repository.

        Returns:
            str: The URL for retrieving the list of collaborators of the GitHub repository.

        Raises:
            None.
        """
        return self._collaborators_url

    @collaborators_url.setter
    def collaborators_url(self, value):
        """Setter method for the `collaborators_url` property.

        Args:
            value (str): The new value for the `collaborators_url` property.

        Returns:
            None.

        Raises:
            None.
        """
        if not isinstance(value, str):
            raise ValueError("URL must be a string.")
        self._collaborators_url = value

    @property
    def teams_url(self):
        """Get the URL for the teams associated with the repository.

        Returns:
            str: The URL for the teams associated with the repository.

        Raises:
            None.
        """
        return self._teams_url

    @teams_url.setter
    def teams_url(self, value):
        """Set the URL for the teams associated with the repository.

        Args:
            value (str): The URL for the teams associated with the repository.

        Returns:
            None.

        Raises:
            None.
        """
        if not isinstance(value, str):
            raise ValueError("URL must be a string.")
        self._teams_url = value

    @property
    def hooks_url(self):
        """str: The URL for managing webhooks on the GitHub repository.

        Returns:
            str: The URL for managing webhooks.

        Raises:
            None.
        """
        return self._hooks_url

    @hooks_url.setter
    def hooks_url(self, value):
        """Sets the hooks URL for managing webhooks on the GitHub repository.

        Args:
            value (str): The new URL for managing webhooks.

        Returns:
            None.

        Raises:
            ValueError: If `value` is not a string.
        """
        if not isinstance(value, str):
            raise ValueError("hooks_url must be a string.")
        self._hooks_url = value

    @property
    def issue_events_url(self):
        """The URL for retrieving issue events associated with the repository.

        Returns:
            str: The URL for retrieving issue events.

        Raises:
            None.
        """
        return self._issue_events_url

    @issue_events_url.setter
    def issue_events_url(self, value):
        """Setter method for the issue_events_url property.

        Args:
            value (str): The new URL for retrieving issue events.

        Returns:
            None.

        Raises:
            TypeError: If the provided value is not a string.
        """
        if not isinstance(value, str):
            raise TypeError("The issue_events_url must be a string.")
        self._issue_events_url = value

    @property
    def events_url(self):
        """The URL for fetching the events related to the repository.

        Returns:
            str: The URL string for fetching the events associated with the repository.

        Raises:
            None.
        """
        return self._events_url

    @events_url.setter
    def events_url(self, url):
        """Setter method for updating the events URL of the repository.

        Args:
            url (str): The new URL for fetching the events associated with the repository.

        Returns:
            None.

        Raises:
            ValueError: If the provided URL is not a valid string.
        """
        if not isinstance(url, str):
            raise ValueError("URL must be a valid string.")
        self._events_url = url

    @property
    def assignees_url(self):
        """The URL template for retrieving the assignees of the repository.

        Returns:
            str: The URL template for retrieving the assignees of the repository.

        Raises:
            None.
        """
        return self._assignees_url

    @assignees_url.setter
    def assignees_url(self, value):
        """Setter method for the `assignees_url` property.

        Args:
            value (str): The URL template for retrieving the assignees of the repository.

        Returns:
            None.

        Raises:
            TypeError: If the `value` argument is not a string.
        """
        if not isinstance(value, str):
            raise TypeError("The assignees_url must be a string.")
        self._assignees_url = value

    @property
    def branches_url(self):
        """Getter for the branches URL.

        Returns:
            str: The URL to retrieve the branches associated with the repository.

        Raises:
            None.
        """
        return self._branches_url

    @branches_url.setter
    def branches_url(self, value):
        """Setter for the branches URL.

        Args:
            value (str): The new URL to set as the branches URL.

        Returns:
            None.

        Raises:
            TypeError: If the provided value is not a string.
        """
        if not isinstance(value, str):
            raise TypeError("The branches URL must be a string.")
        self._branches_url = value

    @property
    def tags_url(self):
        """The URL for retrieving tags associated with the repository.

        Returns:
            str: The URL for retrieving tags.

        Raises:
            None.
        """
        return self._tags_url

    @tags_url.setter
    def tags_url(self, value):
        """Sets the URL for retrieving tags associated with the repository.

        Args:
            value (str): The URL for retrieving tags.

        Returns:
            None.

        Raises:
            None.
        """
        if not isinstance(value, str):
            raise ValueError
        self._tags_url = value

    @property
    def blobs_url(self):
        """The URL for fetching the blobs of the repository.

        Returns:
            str: The URL for fetching the blobs of the repository.

        Raises:
            None.
        """
        return self._blobs_url

    @blobs_url.setter
    def blobs_url(self, value):
        """Setter for the blobs_url property.

        Args:
            value (str): The new value for the blobs_url property.

        Returns:
            None.

        Raises:
            None.
        """
        if not isinstance(value, str):
            raise ValueError("URL must be a string.")
        self._blobs_url = value

    @property
    def git_tags_url(self):
        """The URL for fetching the Git tags associated with the repository.

        Returns:
            str: The URL for fetching the Git tags.

        Raises:
            None.
        """
        return self._git_tags_url

    @git_tags_url.setter
    def git_tags_url(self, value):
        """Setter method for the `git_tags_url` property.

        Args:
            value (str): The new URL for fetching the Git tags.

        Returns:
            None.

        Raises:
            ValueError: If the provided value is not a valid URL.
        """

        if not isinstance(value, str):
            raise ValueError("URL must be a string.")
        if not self.validate_url(value):
            raise ValueError("Invalid URL format for git_tags_url.")
        self._git_tags_url = value

    @property
    def git_refs_url(self):
        """Getter for the Git references URL.

        Returns:
            str: The URL to fetch the Git references of the repository.

        Raises:
            None.
        """
        return self._git_refs_url

    @git_refs_url.setter
    def git_refs_url(self, value):
        """Setter for the Git references URL.

        Args:
            value (str): The URL to set as the Git references URL.

        Returns:
            None.

        Raises:
            TypeError: If the provided value is not a string.
        """

        if not self.validate_url(value):
            raise ValueError("Invalid URL format.")

        if not isinstance(value, str):
            raise TypeError("The git_refs_url must be a string.")
        self._git_refs_url = value

    @property
    def trees_url(self):
        """The URL to fetch the tree data for the repository.

        Returns:
            str: The URL to fetch the tree data for the repository.

        """
        return self._trees_url

    @trees_url.setter
    def trees_url(self, value):
        """Set the URL to fetch the tree data for the repository.

        Args:
            value (str): The new URL to set for fetching the tree data.

        Raises:
            ValueError: If the provided value is not a string.

        """

        if not self.validate_url(value):
            raise ValueError("Invalid URL format.")

        if not isinstance(value, str):
            raise ValueError("trees_url must be a string.")
        self._trees_url = value

    @property
    def statuses_url(self):
        """The URL for fetching the statuses of the repository.

        Returns:
            str: The URL for fetching the statuses of the repository.

        Raises:
            None.
        """
        return self._statuses_url

    @statuses_url.setter
    def statuses_url(self, value):
        """Set the URL for fetching the statuses of the repository.

        Args:
            value (str): The new URL for fetching the statuses of the repository.

        Returns:
            None.

        Raises:
            ValueError: If the provided value is not a string.
        """

        if not self.validate_url(value):
            raise ValueError("Invalid URL format.")

        if not isinstance(value, str):
            raise ValueError("statuses_url must be a string.")
        self._statuses_url = value

    @property
    def languages_url(self):
        """The URL to retrieve the programming languages used in the repository.

        Returns:
            str: The URL to retrieve the programming languages.

        Raises:
            None.
        """
        return self._languages_url

    @languages_url.setter
    def languages_url(self, value):
        """Set the URL to retrieve the programming languages used in the repository.

        Args:
            value (str): The URL to set.

        Returns:
            None.

        Raises:
            None.
        """

        if not isinstance(value, str):
            raise ValueError("URL must be a string.")
        if not self.validate_url(value):
            raise ValueError("Invalid URL format.")

        self._languages_url = value

    @property
    def stargazers_url(self):
        """Get the URL for stargazers of the repository.

        Returns:
            str: The URL for stargazers of the repository.

        """
        return self._stargazers_url

    @stargazers_url.setter
    def stargazers_url(self, url):
        """Set the URL for stargazers of the repository.

        Args:
            url (str): The URL for stargazers of the repository.

        Raises:
            ValueError: If the URL is not a valid string.

        """

        if not self.validate_url(url):
            raise ValueError("Invalid URL format.")

        if not isinstance(url, str):
            raise ValueError("URL must be a valid string.")
        self._stargazers_url = url

    @property
    def contributors_url(self):
        """Get the URL for fetching contributors information.

        Returns:
            str: The URL for fetching the contributors information of the repository.

        Raises:
            None.
        """
        return self._contributors_url

    @contributors_url.setter
    def contributors_url(self, value):
        """Set the URL for fetching contributors information.

        Args:
            value (str): The URL for fetching the contributors information of the repository.

        Returns:
            None.

        Raises:
            TypeError: If the provided value is not a string.
        """
        if not self.validate_url(value):
            raise ValueError("Invalid URL format.")

        if not isinstance(value, str):
            raise TypeError("contributors_url must be a string")
        self._contributors_url = value

    @property
    def subscribers_url(self):
        """Get the URL for the subscribers of the repository.

        Returns:
            str: The URL for the subscribers of the repository.

        """
        return self._subscribers_url

    @subscribers_url.setter
    def subscribers_url(self, value):
        """Set the URL for the subscribers of the repository.

        Args:
            value (str): The URL for the subscribers of the repository.

        Raises:
            ValueError: If the provided value is not a string.

        """
        if not self.validate_url(value):
            raise ValueError("Invalid URL format.")

        if not isinstance(value, str):
            raise ValueError("subscribers_url must be a string.")
        self._subscribers_url = value

    @property
    def subscription_url(self):
        """The subscription URL for the repository.

        Returns:
            str: The URL for managing subscription to the repository.

        Raises:
            ValueError: If the subscription URL is not available or valid.
        """
        return self._subscription_url

    @subscription_url.setter
    def subscription_url(self, value):
        """Set the subscription URL for the repository.

        Args:
            value (str): The new subscription URL for the repository.

        Raises:
            ValueError: If the provided value is not a valid URL.
        """

        if not isinstance(value, str):
            raise ValueError("URL must be a string.")
        if not self.validate_url(value):
            raise ValueError("Invalid URL format.")

        self._subscription_url = value

    @property
    def commits_url(self):
        """The URL for retrieving the commits associated with the repository.

        Returns:
            str: The commits URL of the repository.

        Raises:
            None.
        """
        return self._commits_url

    @commits_url.setter
    def commits_url(self, value):
        """Sets the URL for retrieving the commits associated with the repository.

        Args:
            value (str): The new commits URL to be set.

        Returns:
            None.

        Raises:
            ValueError: If the provided value is not a string.
        """

        if not self.validate_url(value):
            raise ValueError("Invalid URL format.")

        if not isinstance(value, str):
            raise ValueError("commits_url must be a string.")
        self._commits_url = value

    @property
    def issue_comment_url(self):
        """The URL for fetching issue comments from the GitHub API.

        Returns:
            str: The URL to fetch issue comments associated with the repository.

        Raises:
            AttributeError: If the issue_comment_url has not been set.
        """
        if self._issue_comment_url is None:
            raise AttributeError("issue_comment_url has not been set.")
        return self._issue_comment_url

    @issue_comment_url.setter
    def issue_comment_url(self, value):
        """Setter for the issue_comment_url property.

        Args:
            value (str): The URL for fetching issue comments from the GitHub API.
        """

        if not isinstance(value, str):
            raise ValueError("URL must be a string.")
        if not self.validate_url(value):
            raise ValueError("Invalid URL format.")

        self._issue_comment_url = value

    @property
    def contents_url(self):
        """The URL template for retrieving the contents of a repository.

        This property returns the URL template for fetching the contents of a repository using the GitHub API.
        The URL template contains placeholder parameters that can be filled with appropriate values.

        Returns:
            str: The URL template for retrieving the contents of a repository.

        Raises:
            None.
        """
        return self._contents_url

    @contents_url.setter
    def contents_url(self, value):
        """Setter for the contents URL of a repository.

        This setter allows updating the contents URL of a repository with a new value.
        The contents URL is a URL template used for fetching the contents of a repository using the GitHub API.

        Args:
            value (str): The new contents URL to be set for the repository.

        Returns:
            None.

        Raises:
            ValueError: If the provided value is not a string.
        """

        if not self.validate_url(value):
            raise ValueError("Invalid URL format.")

        if not isinstance(value, str):
            raise ValueError("Contents URL must be a string.")
        self._contents_url = value

    @property
    def compare_url(self):
        """The URL for comparing branches or commits.

        Returns:
            str: The compare URL.

        Raises:
            None.
        """
        return self._compare_url

    @compare_url.setter
    def compare_url(self, value):
        """Set the URL for comparing branches or commits.

        Args:
            value (str): The new compare URL.

        Returns:
            None.

        Raises:
            None.
        """
        if not isinstance(value, str):
            raise ValueError("Compare URL must be a string")
        self._compare_url = value

    @property
    def merges_url(self):
        """The URL for retrieving the list of merges of the repository.

        Returns:
            str: The URL for fetching the list of merges associated with the repository.

        Raises:
            None.
        """
        return self._merges_url

    @merges_url.setter
    def merges_url(self, url):
        """Set the URL for retrieving the list of merges of the repository.

        Args:
            url (str): The URL for fetching the list of merges associated with the repository.

        Returns:
            None.

        Raises:
            ValueError: If the provided URL is not a valid string.
        """
        if not isinstance(url, str):
            raise ValueError("Invalid URL. The merges URL must be a string.")
        self._merges_url = url

    @property
    def archives_url(self):
        """The URL for retrieving the list of archives of the repository.

        Returns:
            str: The URL for fetching the list of archives associated with the repository.

        Raises:
            None.
        """
        return self._archives_url

    @archives_url.setter
    def archives_url(self, url):
        """Set the URL for retrieving the list of archives of the repository.

        Args:
            url (str): The URL for fetching the list of archives associated with the repository.

        Returns:
            None.

        Raises:
            ValueError: If the provided URL is not a valid string.
        """
        if not isinstance(url, str):
            raise ValueError("Invalid URL. The archives URL must be a string.")
        self._archives_url = url

    @property
    def downloads_url(self):
        """Getter for the GitHub repository's downloads URL.

        Returns:
            str: The URL for downloading the repository's files.

        """
        return self._downloads_url

    @downloads_url.setter
    def downloads_url(self, value):
        """Setter for the GitHub repository's downloads URL.

        Args:
            value (str): The new downloads URL to be set.

        Raises:
            ValueError: If the provided value is not a valid URL.

        """
        if not isinstance(value, str):
            raise ValueError
        # Additional validation or logic can be added here if necessary
        if not self.validate_url(value):
            raise ValueError("Invalid URL format.")

        self._downloads_url = value

    @property
    def issues_url(self):
        """The URL to retrieve the issues associated with the GitHub repository.

        Returns:
            str: The URL to fetch the issues for the repository.

        Raises:
            None.
        """
        return self._issues_url

    @issues_url.setter
    def issues_url(self, url):
        """Setter method to update the issues URL for the GitHub repository.

        Args:
            url (str): The new URL for fetching the repository's issues.

        Returns:
            None.

        Raises:
            ValueError: If the provided URL is not a valid string.
        """
        if not isinstance(url, str):
            raise ValueError("URL must be a string.")
        self._issues_url = url

    @property
    def pulls_url(self):
        """The URL for retrieving pull requests associated with the repository.

        Returns:
            str: The URL for retrieving pull requests.

        Raises:
            None.

        """
        return self._pulls_url

    @pulls_url.setter
    def pulls_url(self, value):
        """Set the URL for retrieving pull requests associated with the repository.

        Args:
            value (str): The new URL for retrieving pull requests.

        Returns:
            None.

        Raises:
            ValueError: If the provided value is not a string.

        """
        if not isinstance(value, str):
            raise ValueError("Pulls URL must be a string.")
        self._pulls_url = value

    @property
    def milestones_url(self):
        """Getter for the milestones_url property.

        Returns:
            str: The URL for retrieving the milestones associated with the repository.

        Raises:
            None.
        """
        return self._milestones_url

    @milestones_url.setter
    def milestones_url(self, value):
        """Setter for the milestones_url property.

        Args:
            value (str): The URL for retrieving the milestones associated with the repository.

        Returns:
            None.

        Raises:
            TypeError: If the provided value is not a string.
        """
        if not isinstance(value, str):
            raise TypeError("milestones_url must be a string.")
        self._milestones_url = value

    @property
    def notifications_url(self):
        """The URL for accessing the notifications of the repository.

        Returns:
            str: The URL for accessing the notifications of the repository.

        Raises:
            AttributeError: If the notifications URL has not been set.
        """
        if self._notifications_url is None:
            raise AttributeError("Notifications URL has not been set.")
        return self._notifications_url

    @notifications_url.setter
    def notifications_url(self, value):
        """Set the URL for accessing the notifications of the repository.

        Args:
            value (str): The URL for accessing the notifications of the repository.

        Raises:
            ValueError: If the provided value is not a valid URL.
        """
        if not isinstance(value, str):
            raise ValueError("Notifications URL must be a string.")
        # Additional validation logic for URL format can be added here if needed
        self._notifications_url = value

    @property
    def labels_url(self):
        """The URL for retrieving labels associated with the GitHub repository.

        Returns:
            str: The URL for retrieving labels.

        Raises:
            None.
        """
        return self._labels_url

    @labels_url.setter
    def labels_url(self, value):
        """Sets the URL for retrieving labels associated with the GitHub repository.

        Args:
            value (str): The URL for retrieving labels.

        Returns:
            None.

        Raises:
            ValueError: If the provided value is not a string.
        """
        if not isinstance(value, str):
            raise ValueError("The labels_url must be a string.")
        self._labels_url = value

    @property
    def releases_url(self):
        """Getter for the releases URL of the GitHub repository.

        Returns:
            str: The URL for accessing the releases of the GitHub repository.

        Raises:
            None.

        """
        return self._releases_url

    @releases_url.setter
    def releases_url(self, value):
        """Setter for the releases URL of the GitHub repository.

        Args:
            value (str): The new releases URL for the GitHub repository.

        Returns:
            None.

        Raises:
            ValueError: If the provided value is not a string.

        """
        if not isinstance(value, str):
            raise ValueError("Invalid releases URL. Expected a string.")
        self._releases_url = value

    @property
    def deployments_url(self):
        """The URL for fetching deployments related to the repository.

        Returns:
            str: The URL for fetching deployments.

        Raises:
            None.
        """
        return self._deployments_url

    @deployments_url.setter
    def deployments_url(self, value):
        """Setter for the deployments_url property.

        Args:
            value (str): The new URL for fetching deployments.

        Returns:
            None.

        Raises:
            ValueError: If the provided value is not a string.
        """
        if not isinstance(value, str):
            raise ValueError("deployments_url must be a string")
        self._deployments_url = value

    @property
    def mirror_url(self):
        """Get the mirror URL of the repository.

        Returns:
            str or None: The mirror URL of the repository, or None if not available.

        Raises:
            N/A
        """
        return self._mirror_url

    @mirror_url.setter
    def mirror_url(self, value):
        """Set the mirror URL of the repository.

        Args:
            value (str or None): The mirror URL to set for the repository, or None if not available.

        Returns:
            None

        Raises:
            ValueError: If the provided value is not a string or None.
        """
        if value is not None and not isinstance(value, str):
            raise ValueError("Mirror URL must be a string or None.")
        self._mirror_url = value

    @property
    def updated_at(self):
        """The date and time when the repository was last updated.

        Returns:
            str: The ISO 8601 formatted date and time string representing the last update.

        Raises:
            None.
        """
        return self._updated_at

    @updated_at.setter
    def updated_at(self, value):
        """Set the date and time when the repository was last updated.

        Args:
            value (str): The ISO 8601 formatted date and time string representing the last update.

        Returns:
            None.

        Raises:
            ValueError: If the provided value is not a valid ISO 8601 formatted date and time string.
        """

        if not isinstance(value, str):
            raise ValueError("Timestamp format must be a string")
        if not self.is_valid_iso8601_datetime(value):
            raise ValueError("Invalid ISO 8601 formatted date and time string")
        self._updated_at = value

    @property
    def created_at(self):
        """The timestamp indicating the creation date and time of the GitHub repository.

        Returns:
            str: The timestamp formatted as a string in ISO 8601 format.

        Raises:
            None.
        """
        return self._created_at

    @created_at.setter
    def created_at(self, value):
        """Setter for the `created_at` property.

        Args:
            value (str): The timestamp representing the creation date and time of the GitHub repository.
                It should be in ISO 8601 format.

        Returns:
            None.

        Raises:
            ValueError: If the provided `value` is not a valid timestamp in ISO 8601 format.
        """
        if not isinstance(value, str):
            raise ValueError("Timestamp format must be a string")
        if not self.is_valid_iso8601_datetime(value):
            raise ValueError(
                "Invalid timestamp format. Please provide a valid ISO 8601 formatted timestamp."
            )

        self._created_at = value

    @property
    def pushed_at(self):
        """str: The timestamp of the last push to the repository.

        This property represents the timestamp of the most recent push made to the repository. The value is formatted as a string in the ISO 8601 format.

        Returns:
            str: The timestamp of the last push.

        Raises:
            None

        """
        return self._pushed_at

    @pushed_at.setter
    def pushed_at(self, value):
        """Set the timestamp of the last push to the repository.

        Args:
            value (str): The timestamp of the last push in ISO 8601 format.

        Returns:
            None

        Raises:
            ValueError: If the provided value is not a valid ISO 8601 formatted timestamp.

        """
        if not isinstance(value, str):
            raise ValueError("Timestamp format must be a string")

        if not self.is_valid_iso8601_datetime(value):
            raise ValueError(
                "Invalid timestamp format. Please provide a valid ISO 8601 formatted timestamp."
            )

        self._pushed_at = value

    @property
    def git_url(self):
        """Getter for the git URL of the repository.

        Returns:
            str: The git URL of the repository.

        Raises:
            None.
        """
        return self._git_url

    @git_url.setter
    def git_url(self, value):
        """Setter for the git URL of the repository.

        Args:
            value (str): The new git URL of the repository.

        Returns:
            None.

        Raises:
            ValueError: If the provided value is not a valid git URL.
        """
        if not isinstance(value, str):
            raise ValueError("Git URL must be a string.")
        if not self.validate_url(value):
            raise ValueError("Invalid git URL.")
        self._git_url = value

    @property
    def ssh_url(self):
        """The SSH URL of the GitHub repository.

        Returns:
            str: The SSH URL of the repository.

        Raises:
            AttributeError: If the SSH URL is not available.
        """
        return self._ssh_url

    @ssh_url.setter
    def ssh_url(self, value):
        """Set the SSH URL of the GitHub repository.

        Args:
            value (str): The new SSH URL to set.

        Raises:
            ValueError: If the provided value is not a valid SSH URL.
        """
        if not isinstance(value, str):
            raise ValueError("SSH URL must be a string")
        if not self.validate_url(value):
            raise ValueError("Invalid SSH URL provided.")
        self._ssh_url = value

    @property
    def clone_url(self):
        """The clone URL of the GitHub repository.

        Returns:
            str: The clone URL of the repository.

        Raises:
            AttributeError: If the clone URL is not available.
        """
        return self._clone_url

    @clone_url.setter
    def clone_url(self, value):
        """Sets the clone URL of the GitHub repository.

        Args:
            value (str): The clone URL to set.

        Raises:
            ValueError: If the provided clone URL is invalid or empty.
        """
        if not value:
            raise ValueError("Invalid clone URL. Please provide a non-empty URL.")
        if not isinstance(value, str):
            raise ValueError("Clone URL must be a string")
        self._clone_url = value

    @property
    def homepage(self):
        """The homepage URL of the GitHub repository.

        Returns:
            str: The URL of the repository's homepage.

        Raises:
            AttributeError: If the homepage URL is not available.
        """
        return self._homepage

    @homepage.setter
    def homepage(self, value):
        """Set the homepage URL of the GitHub repository.

        Args:
            value (str): The URL of the repository's homepage.

        Raises:
            ValueError: If the provided value is not a valid URL.
        """
        if not isinstance(value, str):
            raise ValueError("Homepage must be a string.")

        self._homepage = value

    @property
    def size(self):
        """The size of the repository.

        Returns:
            int: The size of the repository in kilobytes.

        Raises:
            None.
        """
        return self._size

    @size.setter
    def size(self, value):
        """Set the size of the repository.

        Args:
            value (int): The new size of the repository in kilobytes.

        Returns:
            None.

        Raises:
            ValueError: If the provided value is not an integer or is negative.
        """
        if not isinstance(value, int) or value < 0:
            raise ValueError("Size must be a non-negative integer.")
        self._size = value

    @property
    def archived(self):
        """Getter for the 'archived' property.

        Description:
            Determines if the repository is archived.

        Returns:
            bool: True if the repository is archived, False otherwise.

        Raises:
            None.
        """
        return self._archived

    @archived.setter
    def archived(self, value):
        """Setter for the 'archived' property.

        Description:
            Sets the value of the 'archived' property.

        Args:
            value (bool): The new value for the 'archived' property.

        Returns:
            None.

        Raises:
            None.
        """
        if not isinstance(value, bool):
            raise ValueError("Value must be a boolean.")
        self._archived = value

    @property
    def disabled(self):
        """Whether the repository is disabled.

        Returns:
            bool: True if the repository is disabled, False otherwise.
        """
        return self._disabled

    @disabled.setter
    def disabled(self, value):
        """Set the disabled status of the repository.

        Args:
            value (bool): The new value for the disabled status.

        Raises:
            ValueError: If the provided value is not a boolean.

        Returns:
            None
        """
        if not isinstance(value, bool):
            raise ValueError("Value must be a boolean.")
        self._disabled = value

    @property
    def allow_forking(self):
        """Gets the flag indicating if forking is allowed for the repository.

        Returns:
            bool: True if forking is allowed, False otherwise.
        """
        return self._allow_forking

    @allow_forking.setter
    def allow_forking(self, value):
        """Sets the flag indicating if forking is allowed for the repository.

        Args:
            value (bool): The flag indicating if forking is allowed.

        Raises:
            ValueError: If the provided value is not a boolean.
        """
        if not isinstance(value, bool):
            raise ValueError("The 'allow_forking' value must be a boolean.")
        self._allow_forking = value

    @property
    def web_commit_signoff_required(self):
        """Whether web commit signoff is required for the repository.

        Returns:
            bool: True if web commit signoff is required, False otherwise.

        Raises:
            None.
        """
        return self._web_commit_signoff_required

    @web_commit_signoff_required.setter
    def web_commit_signoff_required(self, value):
        """Set the web commit signoff requirement for the repository.

        Args:
            value (bool): The value indicating whether web commit signoff is required.

        Returns:
            None.

        Raises:
            TypeError: If the provided value is not a boolean.
        """
        if not isinstance(value, bool):
            raise TypeError("Expected a boolean value for web_commit_signoff_required.")
        self._web_commit_signoff_required = value

    @property
    def forks(self):
        """The number of forks of the GitHub repository.

        Description:
            This property represents the count of forks for the GitHub repository.
            A fork is a copy of a repository made by another user to propose changes
            or experiment without affecting the original repository.

        Returns:
            int: The number of forks.

        Raises:
            None.

        """

        return self._forks

    @forks.setter
    def forks(self, value):
        """Setter method for the forks property.

        Description:
            This method sets the number of forks for the GitHub repository.
            It allows modifying the count of forks for the repository.

        Args:
            value (int): The new value for the forks count.

        Returns:
            None.

        Raises:
            None.

        """
        if not isinstance(value, int):
            raise ValueError("The 'forks' value must be an integer.")
        self._forks = value

    @property
    def stargazers_count(self):
        """Get the number of stargazers for the repository.

        Returns:
            int: The count of stargazers for the repository.

        """
        return self._stargazers_count

    @stargazers_count.setter
    def stargazers_count(self, count):
        """Set the number of stargazers for the repository.

        Args:
            count (int): The count of stargazers for the repository.

        Raises:
            ValueError: If the provided count is negative or not int.

        """
        if not isinstance(count, int):
            raise ValueError("Stargazers count must be an integer")
        if count < 0:
            raise ValueError("Stargazers count cannot be negative.")
        self._stargazers_count = count

    @property
    def watchers_count(self):
        """Number of watchers for the GitHub repository.

        Returns:
            int: The count of users who are watching the repository.

        Raises:
            None.
        """
        return self._watchers_count

    @watchers_count.setter
    def watchers_count(self, value):
        """Setter for the number of watchers for the GitHub repository.

        Args:
            value (int): The new count of users watching the repository.

        Returns:
            None.

        Raises:
            ValueError: If the provided value is not a non-negative integer.
        """
        if not isinstance(value, int) or value < 0:
            raise ValueError("Watchers count must be a non-negative integer.")
        self._watchers_count = value

    @property
    def language(self):
        """The primary programming language used in the repository.

        Returns:
            str: The primary programming language of the repository.

        Raises:
            None.
        """
        return self._language

    @language.setter
    def language(self, value):
        """Set the primary programming language of the repository.

        Args:
            value (str): The new primary programming language.

        Returns:
            None.

        Raises:
            ValueError: If the provided value is not a string.
        """
        if not isinstance(value, str):
            raise ValueError("Language must be a string.")
        self._language = value

    @property
    def forks_count(self):
        """Returns the number of forks for the repository.

        Returns:
            int: The number of forks for the repository.
        """
        return self._forks_count

    @forks_count.setter
    def forks_count(self, value):
        """Sets the number of forks for the repository.

        Args:
            value (int): The new number of forks for the repository.

        Raises:
            ValueError: If the provided value is negative.
        """
        if not isinstance(value, int):
            raise ValueError("Forks count must be an integer")
        if value < 0:
            raise ValueError("Number of forks cannot be negative.")
        self._forks_count = value

    @property
    def has_projects(self):
        """Check if the repository has projects enabled.

        Returns:
            bool: True if projects are enabled for the repository, False otherwise.
        """
        return self._has_projects

    @has_projects.setter
    def has_projects(self, value):
        """Set the status of projects for the repository.

        Args:
            value (bool): The new status of projects for the repository.

        Raises:
            ValueError: If the provided value is not a boolean.

        """
        if not isinstance(value, bool):
            raise ValueError("The value must be a boolean.")
        self._has_projects = value

    @property
    def has_wiki(self):
        """Check if the repository has a wiki.

        Returns:
            bool: True if the repository has a wiki, False otherwise.
        """
        return self._has_wiki

    @has_wiki.setter
    def has_wiki(self, value):
        """Set the value indicating if the repository has a wiki.

        Args:
            value (bool): The new value for the 'has_wiki' property.

        Raises:
            ValueError: If 'value' is not a boolean.
        """
        if not isinstance(value, bool):
            raise ValueError("The 'has_wiki' property must be a boolean value.")
        self._has_wiki = value

    @property
    def has_downloads(self):
        """Gets the status of downloads availability.

        Returns:
            bool: True if downloads are enabled for the repository, False otherwise.

        Raises:
            None.
        """
        return self._has_downloads

    @has_downloads.setter
    def has_downloads(self, value):
        """Sets the status of downloads availability.

        Args:
            value (bool): The new value to set for the downloads availability status.

        Returns:
            None.

        Raises:
            ValueError: If an invalid value is provided.
        """
        if not isinstance(value, bool):
            raise ValueError("Invalid value. Downloads availability must be a boolean.")

        self._has_downloads = value

    @property
    def private(self):
        """Gets the privacy status of the repository.

        Returns:
            bool: True if the repository is private, False if it is public.

        Raises:
            None.
        """
        return self._private

    @private.setter
    def private(self, value):
        """Sets the privacy status of the repository.

        Args:
            value (bool): The new privacy status of the repository.

        Returns:
            None.

        Raises:
            TypeError: If the provided value is not a boolean.
        """
        if not isinstance(value, bool):
            raise TypeError("The privacy status must be a boolean.")
        self._private = value

    def validate_url(self, url):
        """Validate if the given string is a valid URL.

        Args:
            url (str): The URL string to validate.

        Returns:
            bool: True if the URL is valid, False otherwise.

        Raises:
            None.
        """
        url_pattern = re.compile(
            r"^(https?|http|ftp)://"  # Scheme
            r"(?:(?:[A-Z0-9][A-Z0-9-]{0,61}[A-Z0-9]\.)+[A-Z]{2,6}|"  # Domain
            r"localhost|"  # localhost
            r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})"  # IPv4
            r"(?::\d+)?"  # Port
            r"(?:/?|[/?]\S+)$",
            re.IGNORECASE,
        )

        return bool(re.match(url_pattern, url))

    def is_valid_iso8601_datetime(self, _value) -> str | None:
        """Check if the given string is a valid ISO 8601 formatted date and time.

        Args:
            _value (str): The string to be checked.

        Returns:
            bool: True if the string is a valid ISO 8601 formatted date and time, False otherwise.
        """
        # Regular expression pattern to match ISO 8601 date and time format
        pattern = (
            r"^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(?:\.\d+)?(?:Z|[+-]\d{2}:\d{2})$"
        )
        if isinstance(_value, str):
            ValueError("_value must be a string")
        if bool(re.match(pattern, _value)):
            return _value

    def prettify(self):
        """Print the repostiory details in an aesthetic way"""

    @classmethod
    def check_repo_url_ping(cls, _url: str) -> bool:
        """Check url via requests status check.

        :param url: Url as any valid formatted property URL.
        :type url: str

        :return: Status code of ping is 200 or not 200
        :rtype: bool
        """

        with cls.console.status(f"Getting status code from {_url} "):
            _r = requests.get(_url)
            _up = _r.status_code == 200

            return _up

    def check_repo_meets_criteria(self, stars=[0, 10]) -> bool:
        """Check that the repository meets criteria.

        Args:
            stars list[int | bool, int | bool]: The range of stars allowed on the repository.
                stars[0] represents the minimum allowed number of stars.
                stars[1] represents the maximum allowed number of stars.

                Criteria
                --------
                If stars[1] is equal to True then the amount of stars
                is dependant on criteria of stars = >= stars[0]
                If stars[0] is equal to True then the amount of stars is
                dependant on criteria of stars = <= stars[1]

                Notes
                ------
                numerical range is INclusive on both sides.
                * Must follow configuration of [int, bool] or [bool, int]

         Returns: Literal[True] | Literal[False]
        """

        if self._private is True:
            return False
        if self.open_issues_count == 0:
            return False

        # NOTE: Done last to prevent assining local variables
        if isinstance(stars[1], bool) and stars[1]:
            return self.stargazers_count >= stars[0]
        elif isinstance(stars[0], bool) and stars[0]:
            return self.stargazers_count <= stars[1]
        elif isinstance(stars[1], int) and isinstance(stars[0], int):
            return self.stargazers_count in range(*stars)

        return True

    def shorten(self, limit=50) -> str:
        """Shorten description if it's longer than `limit`.

        A string that is longer than `limit` will
        have an ellipsis postfix.

        :param string: String to shorten, length can be any length
        :param limit: The limit of characters to shorten with
        :return: String as normal otherwise with a elipses suffix
        :rtype: str
        """

        _string_ = ""
        if isinstance(self.description, str):
            if len(self.description) > limit:
                _string_ = self.description[:limit] + "..."
            elif self.description != "":
                _string_ = self.description
        else:
            return "No description available :("
        return _string_


if __name__ == "__main__":
    declare_basic_config()
