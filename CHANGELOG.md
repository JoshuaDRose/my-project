## (2023-05-19)

**0.1.0 ALPHA RELEASE**
 - First alpha release of all the previous changes into !2 and then into a release!
 - Before the release additional checks such as typing for properties, code optimization and removing redundant tests were all things that were worked on to make sure the release was stable.

## (2023-05-18)

**Much improved aesthetic for showing repos**
 - The functionality may not be there yet, but the eye candy sure it! Bringing in the Rich library to add some spice to the visuals.
 - Not much got done today, I was mainly configuring my i3 to look as good as vcs does now.

**Changes to Properties/Repository class **
 - Add additional type checks to `Repository` properties (_including urls and date checks_
 - adds stat method to determine (as boolean) if site response code is 200 (tests included for this as well)

**Fixes / CI stuff**
 - added rich to Pytest module for tox deps so that now runs without failing (alternative to running pytest w/o tox)

## (2023-05-17)

### **Integration of new stuff** (VERSION 0.1.0)
**Fixes**
 - the operator comparative class was removed as it's unneeded for release 0.1.0
 - omits classes that were planned to be implemented in release 0.0.2
 - fixed issue where repositories would only be added and shown if `NameError` occurred

**Features**
 - Because 'sort' is a mandatory argument in the GitHub API I figured that this should just be an argument anyway because it grants the user more control over the API.
 - More aesthetic listing of results using bar character

*Breaking Changes!*
 - Shortened `--description` to `--desc` so that when the `--help` command is called it doesn't overflow into the command description

__note__:
> currently working on adding properties to the repository class! 🙈

\* __also new stuff for the wiki__

## (2023-05-15)

**Dependencies**
 - Revoked tqdm from dependencies (as of commits after 40ca3996)

**Even more stuff!**
 - You can now sort repos by stars 💫

**Query command**
 - Implement query command: index issues by a phrase
 - Removed redundant version argument (-V) and now its just --version

**Initial command line**
 - Implemented the argparse library: Now using the argparse library to help with commands and all that jazz.
 - Added a version command to the command line: A new version command (-V and --version) now work :)

**Feat: new tests**
 - Added tests to go alongside this implementation: add 2 tests (more soon); deleted a test for the tests (haha)

##  (2023-05-14)

**Create the project**
 - Add initial files such as the `CHANGELOG`, `LICENSE` and other standard project files

**Add CI and testing framework**
 - Work on continuous integration: I worked a bit today on the tests that I plan to implement tomorrow or the next day.
 - Implement tox testing: These tests now work with the tox development environment and any tests made should reflect that.
 - Add pages development: In addition to this, a pages' deployment should be active and constantly generating through the 'Sphinx' library.
