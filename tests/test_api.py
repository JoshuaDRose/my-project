"""
VCS Issue Finder - Find issues quickly
Copyright © 2023 Cyré
Licensed under the MIT open source software license.

Author: Cyré
File: tests/api.py
"""

import requests

from vcs.api import GitHub
from vcs.utils.repository import Repository  # pyright: ignore


def test_find_issues_has_items_key():
    issues_response: requests.Response = requests.get(
        GitHub.SEARCH_URL,
        params={"q": "test", "sort": "best_match", "order": "desc"},
    )

    issues_dict = issues_response.json()

    assert "items" in issues_dict.keys()


def test_repository_value():
    """Assert repository contains valid and correct data and types"""
