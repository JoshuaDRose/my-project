"""
VCS Issue Finder - Find issues quickly
Copyright © 2023 Cyré
Licensed under the MIT open source software license.

Author: Cyré
File: tests/test_parser.py
"""

import conf  # pyright: ignore
from vcs.utils import Parser


def test_parser_version_is_conf():
    """
    Asserts that the version in conf.py is equal to parser properties.
    """
    parser = Parser()
    assert parser.configuration.version == conf.release
