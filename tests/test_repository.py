"""
VCS Issue Finder - Find issues quickly
Copyright © 2023 Cyré
Licensed under the MIT open source software license.

Author: Cyré
File: tests/test_repository.py
"""

import random
import unittest

from vcs.api import GitHub
from vcs.utils import Repository


class TestRepository(unittest.TestCase):
    repository_fetch: list[Repository] = GitHub.find_issues(
        "test", "best_match", "desc", test_case=True
    )

    def setUp(self) -> None:
        self.repo = random.choice(TestRepository.repository_fetch)
        return super().setUp()

    def tearDown(self) -> None:
        del self.repo
        return super().tearDown()

    def test_check_repo_url_ping(self):
        # print("Testing repo check url, (https://example.com/) status is 200?")
        result = self.repo.check_repo_url_ping("https://example.com")
        assert result

    def test_property_id(self):
        assert isinstance(self.repo.id, int)
        assert len(str(self.repo.id)) in range(5, 15)

    def test_property_node_id(self):
        assert isinstance(self.repo.node_id, str)

    def test_property_license(self):
        assert isinstance(self.repo.license, dict | None)

    def test_property_name(self):
        assert isinstance(self.repo.name, str)

    def test_property_full_name(self):
        assert isinstance(self.repo.full_name, str)

    def test_property_default_branch(self):
        assert isinstance(self.repo.default_branch, str)

    def test_property_open_issues_count(self):
        assert isinstance(self.repo.open_issues_count, int)

    def test_property_html_url(self):
        assert isinstance(self.repo.html_url, str)

    def test_property_description(self):
        assert isinstance(self.repo.description, str)

    def test_property_fork(self):
        assert isinstance(self.repo.fork, bool)

    def test_property_topics(self):
        assert isinstance(self.repo.topics, list)

    def test_property_url(self):
        assert isinstance(self.repo.url, str)

    def test_property_forks_url(self):
        assert isinstance(self.repo.forks_url, str)

    def test_property_keys_url(self):
        assert isinstance(self.repo.keys_url, str)

    def test_property_collaborators_url(self):
        assert isinstance(self.repo.collaborators_url, str)

    def test_property_teams_url(self):
        assert isinstance(self.repo.teams_url, str)

    def test_property_hooks_url(self):
        assert isinstance(self.repo.hooks_url, str)

    def test_property_issue_events_url(self):
        assert isinstance(self.repo.issue_events_url, str)

    def test_property_events_url(self):
        assert isinstance(self.repo.events_url, str)

    def test_property_assignees_url(self):
        assert isinstance(self.repo.assignees_url, str)

    def test_property_branches_url(self):
        assert isinstance(self.repo.branches_url, str)

    def test_property_tags_url(self):
        assert isinstance(self.repo.tags_url, str)

    def test_property_blobs_url(self):
        assert isinstance(self.repo.blobs_url, str)

    def test_property_git_tags_url(self):
        assert isinstance(self.repo.git_tags_url, str)

    def test_property_git_refs_url(self):
        assert isinstance(self.repo.git_refs_url, str)

    def test_property_trees_url(self):
        assert isinstance(self.repo.trees_url, str)

    def test_property_statuses_url(self):
        assert isinstance(self.repo.statuses_url, str)

    def test_property_languages_url(self):
        assert isinstance(self.repo.languages_url, str)

    def test_property_stargazers_url(self):
        assert isinstance(self.repo.stargazers_url, str)

    def test_property_contributors_url(self):
        assert isinstance(self.repo.contributors_url, str)

    def test_property_subscribers_url(self):
        assert isinstance(self.repo.subscribers_url, str)

    def test_property_commits_url(self):
        assert isinstance(self.repo.commits_url, str)

    def test_property_issue_comment_url(self):
        assert isinstance(self.repo.issue_comment_url, str)

    def test_property_contents_url(self):
        assert isinstance(self.repo.contents_url, str)

    def test_property_compare_url(self):
        assert isinstance(self.repo.compare_url, str)

    def test_property_merges_url(self):
        assert isinstance(self.repo.merges_url, str)

    def test_property_downloads_url(self):
        assert isinstance(self.repo.downloads_url, str)

    def test_property_issues_url(self):
        assert isinstance(self.repo.issues_url, str)

    def test_property_pulls_url(self):
        assert isinstance(self.repo.pulls_url, str)

    def test_property_milestones_url(self):
        assert isinstance(self.repo.milestones_url, str)

    def test_property_notifications_url(self):
        assert isinstance(self.repo.notifications_url, str)

    def test_property_labels_url(self):
        assert isinstance(self.repo.labels_url, str)

    def test_property_releases_url(self):
        assert isinstance(self.repo.releases_url, str)

    def test_property_deployments_url(self):
        assert isinstance(self.repo.deployments_url, str)

    def test_property_mirror_url(self):
        assert isinstance(self.repo.mirror_url, None | str)

    def test_property_updated_at(self):
        assert isinstance(self.repo.updated_at, str)

    def test_property_created_at(self):
        assert isinstance(self.repo.created_at, str)
        assert self.repo.is_valid_iso8601_datetime(self.repo.created_at)

    def test_property_pushed_at(self):
        assert isinstance(self.repo.pushed_at, str)
        assert self.repo.is_valid_iso8601_datetime(self.repo.pushed_at)

    def test_property_git_url(self):
        assert isinstance(self.repo.git_url, str)
        # assert self.repo.validate_url(self.repo.git_url)

    def test_property_ssh_url(self):
        assert isinstance(self.repo.ssh_url, str)
        # assert self.repo.validate_url(self.repo.ssh_url)

    def test_property_clone_url(self):
        assert isinstance(self.repo.clone_url, str)
        # assert self.repo.validate_url(self.repo.clone_url)

    def test_property_homepage(self):
        assert isinstance(self.repo.homepage, str | None)

    def test_property_size(self):
        assert isinstance(self.repo.size, int)

    def test_property_archived(self):
        assert isinstance(self.repo.archived, bool)

    def test_property_allow_forking(self):
        assert isinstance(self.repo.allow_forking, bool)

    def test_property_web_commit_signoff_required(self):
        assert isinstance(self.repo.web_commit_signoff_required, bool)

    def test_property_stargazers_count(self):
        assert isinstance(self.repo.stargazers_count, int)

    def test_property_watchers_count(self):
        assert isinstance(self.repo.watchers_count, int)

    def test_property_language(self):
        assert isinstance(self.repo.language, str)

    def test_property_forks_count(self):
        assert isinstance(self.repo.forks_count, int)

    def test_property_has_projects(self):
        assert isinstance(self.repo.has_projects, bool)

    def test_property_has_wiki(self):
        assert isinstance(self.repo.has_wiki, bool)

    def test_property_has_downloads(self):
        assert isinstance(self.repo.has_downloads, bool)

    def test_property_private(self):
        assert self.repo.private == False
