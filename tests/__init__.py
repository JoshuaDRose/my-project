"""
VCS Issue Finder - Find issues quickly
Copyright © 2023 Cyré
Licensed under the MIT open source software license.

Author: Cyré
File: tests/__init__.py
"""

import os
import sys

sys.path.insert(0, os.path.abspath(os.getcwd()))

import vcs.utils  # pyright: ignore


def test_python_path():
    """Test that the python path is valid.

    Upon loading the 'tests' folder, this function is run,
    which ensures that the python folder is working and the
    current working directory is valid.

    This ensures that the src folder is recognized and, ultimately,
    ensures tests work as they are meant to and do not fail.
    """

    dir_name = os.path.dirname(os.getcwd())
    if __name__ != "__main__":
        sys.path.insert(0, dir_name)
        assert dir_name == "my-project"
    assert dir_name == "my-project"
