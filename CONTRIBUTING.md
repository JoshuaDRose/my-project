# Contributing

Thank you for your interest in contributing to VCS Issue Finder!

Please follow these guidelines to make the contribution process easy and effective for everyone involved.

## How to Contribute

1. Fork the repository and create your branch from `main`.
2. Make your changes and ensure that your code adheres to our style guide.
3. Test your changes locally.
4. Create a pull request with a clear description of your changes and the problem they solve.

## Style Guide

- Use [Black](https://pypi.org/project/black/) to ensure consistent line endings and indentation.
- Write clear, concise, and meaningful commit messages. 
- Create tests along side your contribution to improve code coverage

## Reporting Bugs

If you encounter a bug, please open an issue on the project's issue tracker with a clear description of the problem and steps to reproduce it. 
